import React from "react";

export default function ResetFilters({ handleResetFilter }) {
  return (
    <div class="facet_wrap">
      <button onClick={handleResetFilter}>Reset Filters</button>
    </div>
  );
}
